package univ.dlabs.Services;

import org.springframework.stereotype.Service;
import univ.dlabs.DO.DealDO;
import univ.dlabs.DO.TemperatureDO;

@Service
public class ProcessDegreeBO {

    public Integer getDegree(final DealDO dealDO) {
        Integer result = 0;

        for (final TemperatureDO element : dealDO.getTemperatures()) {
            result += element.getValue();
        }

        return result;
    }
}
