package univ.dlabs.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.dlabs.DAO.IDealRepository;
import univ.dlabs.DAO.IUserRepository;
import univ.dlabs.DO.DealDO;
import univ.dlabs.DTO.SingleDealDTO;

import java.util.Optional;

@Service
public class SingleDealBO {

    @Autowired
    private IDealRepository dealDAO;

    @Autowired
    private ProcessDegreeBO processDegreeBO;

    @Autowired
    private DealMapper dealMapper;

    @Autowired
    private IUserRepository userDAO;

    public SingleDealDTO findOne(final Integer id) {
        final Optional<DealDO> dealDO = dealDAO.findById(id);
        if (dealDO.isPresent()) {
            final SingleDealDTO singleDealDTO = dealMapper.dealDOToSingleDealDTO(dealDO.get());
            singleDealDTO.setDegree(processDegreeBO.getDegree(dealDO.get()));
            return singleDealDTO;
        }
        return null;
    }

    public SingleDealDTO save(final SingleDealDTO singleDealDTO) {
        final DealDO dealDO = dealMapper.singleDealDTOToDealDO(singleDealDTO);
        dealDO.setCreator(userDAO.findUserWithName(singleDealDTO.getAuthor()));
        final DealDO updatedDealDO = dealDAO.save(dealDO);
        singleDealDTO.setId(updatedDealDO.getId());
        return singleDealDTO;
    }
}
