package univ.dlabs.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import univ.dlabs.DAO.IDealRepository;
import univ.dlabs.DO.DealDO;
import univ.dlabs.DTO.DealDTO;

import java.util.*;

@Service
public class DealBO {

    @Autowired
    private IDealRepository dealDAO;

    @Autowired
    private DealMapper dealMapper;

    @Autowired
    private ProcessDegreeBO processDegreeBO;

    public List<DealDTO> findAll() {
        final List<DealDO> dealDOArray = dealDAO.findAll();
        final List<DealDTO> dealDTOArray = new ArrayList<DealDTO>();
        Collections.sort(dealDOArray, Comparator.comparing(DealDO::getDate).reversed());
        for(final DealDO element : dealDOArray) {
            dealDTOArray.add(dealMapper.dealDOToDealDTO(element));
            dealDTOArray.get(dealDTOArray.size()-1).setDegree(processDegreeBO.getDegree(element));
        }
        return dealDTOArray;
    }
}
