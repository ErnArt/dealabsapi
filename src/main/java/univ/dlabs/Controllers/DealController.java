package univ.dlabs.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import univ.dlabs.DTO.DealDTO;
import univ.dlabs.DTO.SingleDealDTO;
import univ.dlabs.Services.DealBO;
import univ.dlabs.Services.SingleDealBO;

import java.util.List;

@RestController
@RequestMapping(value = "/api/Deal")
public class DealController {
    @Autowired
    private DealBO dealBO;

    @Autowired
    private SingleDealBO singleDealBO;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "public", method = RequestMethod.GET)
    public List<DealDTO> findAll() {
        return dealBO.findAll();
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.POST)
    public SingleDealDTO save(@RequestBody final SingleDealDTO dealDTO) {
        return singleDealBO.save(dealDTO);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "public/{id}", method = RequestMethod.GET)
    public SingleDealDTO findOne(@PathVariable final Integer id) { return singleDealBO.findOne(id); }
}