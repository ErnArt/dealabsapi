package univ.dlabs.Controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import univ.dlabs.DTO.LoginRequestDTO;
import univ.dlabs.DTO.ShortUserDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MDE
 *
 */
 // TODO : A decommenter quand le code compilera
//@RestController
//@RequestMapping(value = "/public/bd/login")
//@Transactional
public class LoginBD {

	private static final Logger logger = LoggerFactory.getLogger(LoginBD.class);

	@Autowired
	private AuthenticationProvider authenticationManager;


	/**
	 * methode de connexion d'un utilisateur
	 *
	 * @param request données necessaire a la connexion
	 * @return
	 * @throws RestException
	 * @throws Exception
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ShortUserDTO login(@RequestBody final LoginRequestDTO request, final HttpServletRequest req) {

		// Controle des params obligatoires
		if (StringUtils.isEmpty(request.getUsername()) || StringUtils.isEmpty(request.getPassword())) {
			throw new BadCredentialsException("User/pwd must not be emtpy");

		}
		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
		if (authentication == null) {
			throw new BadCredentialsException("User/pwd incorrect");
		}

		final ShortUserDTO utilisateur = (ShortUserDTO) authentication.getPrincipal();
		logger.debug(String.format("New user logged : %s", utilisateur.getUsername()));

		// TODO : Ajouter les informations n�cessaires au DTO pour retour au front.

		final List<GrantedAuthority> grantedAuths = new ArrayList<>();
		return new ShortUserDTO(request.getUsername(), request.getPassword(), grantedAuths);
	}

}
