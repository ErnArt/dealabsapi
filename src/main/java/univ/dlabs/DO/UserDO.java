package univ.dlabs.DO;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TBL_USER")
public class UserDO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer id;

    @Column(name = "PSEUDO", unique = true)
    private String pseudo;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "PASSWORD")
    private String password;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "user")
    private List<TemperatureDO> temperatures;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "creator")
    private List<DealDO> deals;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getPseudo() {
        return this.pseudo;
    }

    public void setPseudo(final String pseudo) {
        this.pseudo = pseudo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public List<TemperatureDO> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(final List<TemperatureDO> temperatures) {
        this.temperatures = temperatures;
    }

    public List<DealDO> getDeals() {
        return deals;
    }

    public void setDeals(final List<DealDO> deals) {
        this.deals = deals;
    }
}
