package univ.dlabs.DO;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "TBL_DEAL")
public class DealDO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;

    @Column(name="TITLE")
    private String title;

    @Column(name = "SHOP_NAME")
    private String company;

    @Column(name = "SHOP_LINK")
    private String link;

    @Column(name = "PRICE_OLD")
    private Double priceOld;

    @Column(name = "PRICE_NEW")
    private Double priceNew;

    @Column(name = "PROMO_CODE")
    private String promoCode;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "IMG_URL")
    private String pictureLink;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "FK_CREATOR")
    private UserDO creator;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "deal")
    private List<TemperatureDO> temperatures;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(final String company) {
        this.company = company;
    }

    public String getLink() {
        return link;
    }

    public void setLink(final String link) {
        this.link = link;
    }

    public Double getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(final Double priceOld) {
        this.priceOld = priceOld;
    }

    public Double getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(final Double priceNew) {
        this.priceNew = priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(final String promoCode) {
        this.promoCode = promoCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(final Date date) {
        this.date = date;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(final String pictureLink) {
        this.pictureLink = pictureLink;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public UserDO getCreator() {
        return creator;
    }

    public void setCreator(final UserDO creator) {
        this.creator = creator;
    }

    public List<TemperatureDO> getTemperatures() {
        return temperatures;
    }

    public void setTemperatures(final List<TemperatureDO> temperatures) {
        this.temperatures = temperatures;
    }
}
